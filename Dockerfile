FROM golang:1.22.4-alpine3.20 as builder
RUN go install github.com/pressly/goose/v3/cmd/goose@latest
RUN cp $GOPATH/bin/goose /goose

FROM alpine:3.20
WORKDIR /app/
RUN mkdir data
ENV DRIVER=postgres
ENV CONNECTION_STRING="user=postgres dbname=postgres password=password host=localhost port=5432 sslmode=disable"
ENV COMMAND=up
COPY --from=builder /goose /app/goose
CMD ./goose -dir ./data -v $DRIVER "$CONNECTION_STRING" $COMMAND
